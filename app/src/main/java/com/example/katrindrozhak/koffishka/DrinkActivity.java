package com.example.katrindrozhak.koffishka;

import android.view.View;

public class DrinkActivity extends BaseInfoActivity {

    public void onFavoriteClicked(View view) {

        Drink drink = Drink.findById(Drink.class, itemId);

        drink.setFavorite(cbFavorite.isChecked());
        drink.save();
    }

    @Override
    public void updateUI() {
        super.updateUI();
        Drink drink = Drink.findById(Drink.class, itemId);
        tvName.setText(drink.getName());
        tvDescription.setText(drink.getDescription());
        ivPhoto.setImageResource(drink.getImageResourceId());
        ivPhoto.setContentDescription(drink.getName());
        cbFavorite.setChecked(drink.isFavorite());
        ivBackground.setImageResource(drink.getImageResourceId());
    }
}
