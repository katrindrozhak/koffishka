package com.example.katrindrozhak.koffishka;

import android.view.View;

public class FoodActivity extends BaseInfoActivity {

    public void onFavoriteClicked(View view) {

        Food food = Food.findById(Food.class, itemId);

        food.setFavorite(cbFavorite.isChecked());
        food.save();
    }

    @Override
    public void updateUI() {
        super.updateUI();
        Food food = Food.findById(Food.class, itemId);
        tvName.setText(food.getName());
        ivPhoto.setImageResource(food.getImageResourceId());
        ivPhoto.setContentDescription(food.getName());
        cbFavorite.setChecked(food.isFavorite());
        ivBackground.setImageResource(food.getImageResourceId());
    }
}
