package com.example.katrindrozhak.koffishka;

import android.content.Intent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class FoodCategoryActivity extends BaseCategoryListActivity {

    private List<Food> nameFood;

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Food selectedFood = nameFood.get(position);
        Intent intent = new Intent(FoodCategoryActivity.this, FoodActivity.class);
        intent.putExtra(FoodActivity.ITEM_ID_KEY, selectedFood.getId());
        startActivity(intent);
    }

    public void createList() {

        nameFood = Food.listAll(Food.class);

        ArrayAdapter<Food> listAdapterFood = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, nameFood);
        itemsList.setAdapter(listAdapterFood);
    }
}
