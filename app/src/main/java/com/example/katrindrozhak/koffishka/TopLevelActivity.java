package com.example.katrindrozhak.koffishka;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class TopLevelActivity extends Activity {

    private void fillDB(Context context) {
        // SharedPreferences создает текстовый файл и позволяет нам сохранять настройки
        SharedPreferences preferences = context.getSharedPreferences("preffood",
                Context.MODE_PRIVATE);
        // запрашиваем у  SharedPreferences(хранилище настроек),
        // что у нас хранится по ключу. Если по данному ключу ничего нет -
        // верентся false , а это значит, что приложенние запускается
        // прервый раз и БД еще не создана - можем ее заполнять
        boolean dbCreated = preferences.getBoolean("dbCreatedKey", false);
        if (!dbCreated) {

            Food.fillFood();
            Drink.fillDrink();

            //       после того, как БД зполнена данными сохраняем
            // в SharedPreferences  true по ключу dbCreatedKey,
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("dbCreatedKey", true);

            // а занчит при следующем запуске приложения dbCreated = true ,
            // следовательно данные в БД есть, тело if не выполняется
            editor.apply();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_level);
        fillDB(this);
        optionsItemClick();

    }

    public void optionsItemClick() {
        ListView listOptions = (ListView) findViewById(R.id.options);

        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View itemView,
                                    int position, long id) {
                Intent intent = null;

                switch (position) {
                    case 0:
                        intent = new Intent(TopLevelActivity.this, DrinkCategoryActivity.class);
                        break;
                    case 1:
                        intent = new Intent(TopLevelActivity.this, FoodCategoryActivity.class);
                        break;
                    case 2:
                        intent = new Intent(TopLevelActivity.this, Stores.class);
                        break;
                    case 3:
                        intent = new Intent(TopLevelActivity.this, Favorite.class);
                        break;
                }
                startActivity(intent);
            }
        };

        listOptions.setOnItemClickListener(itemClickListener);
    }

}
