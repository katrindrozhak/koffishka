package com.example.katrindrozhak.koffishka;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ListView;

public class BaseCategoryListActivity extends ListActivity {
    ListView itemsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        itemsList = getListView();
        createList();
    }

    public void createList() {

    }
}
