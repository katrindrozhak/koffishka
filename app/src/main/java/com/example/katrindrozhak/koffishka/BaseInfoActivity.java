package com.example.katrindrozhak.koffishka;

import android.app.Activity;
import android.os.Bundle;

import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import jp.wasabeef.blurry.Blurry;

public class BaseInfoActivity extends Activity {
    // идентификатор напитка выбранного пользователем
    public static final String ITEM_ID_KEY = "ItemNo";

    protected TextView tvName;
    protected TextView tvDescription;
    protected ImageView ivPhoto;
    protected CheckBox cbFavorite;
    protected ImageView ivBackground;

    protected Long itemId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        itemId = (Long) getIntent().getExtras().get(ITEM_ID_KEY);

        prepareUI();
        updateUI();
    }

    private void prepareUI() {
        tvName = (TextView) findViewById(R.id.name);
        tvDescription = (TextView) findViewById(R.id.description);
        ivPhoto = (ImageView) findViewById(R.id.photo);
        cbFavorite = (CheckBox) findViewById(R.id.favorite);
        ivBackground = (ImageView) findViewById(R.id.background_image);
        ivBackground.setAdjustViewBounds(true);
        ivBackground.setScaleType(ImageView.ScaleType.CENTER);

    }

    public void updateUI() {
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        Blurry.with(this).radius(3).sampling(5).async()
                .onto((ViewGroup) findViewById(R.id.layoutBackground));
    }
}
