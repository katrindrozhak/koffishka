package com.example.katrindrozhak.koffishka;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class Favorite extends Activity {
    private ListView listFavoritesDrinks;
    private ListView listFavoriteFoods;
    private List<Drink> nameFavoriteDrink;
    private List<Food> nameFavoriteFood;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favorite_activity);
        infoItemClick();
        updateFavoriteDrink();
        updateFavoriteFood();
    }

    public void infoItemClick() {
        //  заполнение list_favorites данными курсора
        // получить ListView любимых напитков
        listFavoritesDrinks = (ListView) findViewById(R.id.list_favorites_drinks);
        listFavoriteFoods = (ListView) findViewById(R.id.list_favoriteFoods);

        //  переход к DrinkActivity при выборе напитка
        listFavoritesDrinks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intentDrink = new Intent(Favorite.this, DrinkActivity.class);
                Drink selectedFavoriteDrink = nameFavoriteDrink.get(position);
                //  при выборе одонго из вариантов LIstView любимых напитков, создать
                //  интент для запуска DrinkActivity и передать id напитка
                intentDrink.putExtra(DrinkActivity.ITEM_ID_KEY, selectedFavoriteDrink.getId());
                startActivity(intentDrink);
            }
        });

        listFavoriteFoods.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intentFood = new Intent(Favorite.this, FoodActivity.class);
                Food selectedFavoriteFood = nameFavoriteFood.get(position);
                intentFood.putExtra(FoodActivity.ITEM_ID_KEY, selectedFavoriteFood.getId());
                startActivity(intentFood);
            }
        });
    }
    // вызывается при возвращении пользователя к MTopLevelActivity
    @Override
    public void onRestart() {
        super.onRestart();
        updateFavoriteDrink();
        updateFavoriteFood();
    }

    public void updateFavoriteDrink() {
        nameFavoriteDrink = Drink.find(Drink.class, "favorite = ?", "1");
        ArrayAdapter<Drink> adapterFavoritesDrink = new ArrayAdapter<Drink>(
                this, android.R.layout.simple_list_item_1, nameFavoriteDrink);
        listFavoritesDrinks.setAdapter(adapterFavoritesDrink);
    }

    public void updateFavoriteFood() {
        nameFavoriteFood = Food.find(Food.class, "favorite = ?", "1");
        ArrayAdapter<Food> adapterFavoriteFood = new ArrayAdapter<Food>(
                this, android.R.layout.simple_list_item_1, nameFavoriteFood);
        listFavoriteFoods.setAdapter(adapterFavoriteFood);
    }

}
