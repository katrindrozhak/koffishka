package com.example.katrindrozhak.koffishka;

import android.content.Intent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class DrinkCategoryActivity extends BaseCategoryListActivity {

    private List<Drink> nameDrinks;

    public void onListItemClick(ListView listView, View itemview,
                                int position, long id) {
        // при нажатии на ListItem,
        // используя переменную position мы получаем ссылку
        // на соответствующей этой позиции объект типа Drink( из nameDrinks)
        Drink selectedDrink = nameDrinks.get(position);
        Intent intent = new Intent(DrinkCategoryActivity.this, DrinkActivity.class);
        intent.putExtra(DrinkActivity.ITEM_ID_KEY, selectedDrink.getId());
        startActivity(intent);
    }

    public void createList() {

        nameDrinks = Drink.listAll(Drink.class);

        ArrayAdapter<Drink> listAdapterDrinks = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, nameDrinks);
        itemsList.setAdapter(listAdapterDrinks);
    }
}
