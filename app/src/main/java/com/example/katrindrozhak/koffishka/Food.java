package com.example.katrindrozhak.koffishka;

import com.orm.SugarRecord;

public class Food extends SugarRecord {
    private String name;
    private int imageResourceId;
    private boolean favorite;

    public Food() {
    }

    public Food(String name, int imageResourceId) {
        this.name = name;
        this.imageResourceId = imageResourceId;
    }

    public static void fillFood() {
        new Food("Cookie mix", R.drawable.cookie_mix).save();
        new Food("Nougat", R.drawable.nougat).save();
        new Food("Brownie", R.drawable.brownie).save();
        new Food("Macaroons", R.drawable.macaroon).save();
        new Food("Holland waffles", R.drawable.holand_waffles).save();
        new Food("Bizet", R.drawable.bize).save();
        new Food("Cheese cake", R.drawable.cheese_cake).save();
        new Food("Apple strudel", R.drawable.apple_strudel).save();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
}
