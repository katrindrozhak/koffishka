package com.example.katrindrozhak.koffishka;

import com.orm.SugarRecord;

/**
 * Created by katrindrozhak on 16.09.16.
 */

public class Drink extends SugarRecord {

    private String name;
    private String description;
    private int imageResourceId;
    private boolean favorite;

    public Drink() {
    }

    public Drink(String name, String description, int imageResourceId) {
        this.name = name;
        this.description = description;
        this.imageResourceId = imageResourceId;
    }

    public static void fillDrink() {
        new Drink("Latte", "Espresso shots with steamed milk",
                R.drawable.latte).save();
        new Drink("Cappuccino", "Espresso, hot milk, and a steamed milk foam",
                R.drawable.cappuccino_coffee).save();
        new Drink("Filter", "Highest quality beans roasted and brewed fresh",
                R.drawable.filter).save();
        new Drink("Americano", "double coffee", R.drawable.americano).save();
        new Drink("Espresso", "shot coffee", R.drawable.espresso).save();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
}
